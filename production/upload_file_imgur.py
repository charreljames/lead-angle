from imgurpython import ImgurClient
import random
import os


client_api = [
    ["9825d47ce5d48e0","5ac1a7913d736efb0fabc54749060e34faec942d"]

]

client_instance = random.choice(client_api)
client_id, client_secret = client_instance

client = ImgurClient(client_id, client_secret)


def writeTextFile(textlist):
    id = random.randrange(1000,9000)
    filename = "results/{}.txt".format(id)
    with open(filename, "w") as f:
        for i in textlist:
            f.write(i + "\n")
        f.close()

    print("CHECK THE TEXTFILE : {}".format(filename))


def upload_image( upload_file ):
    hash = ""
    try:
        res = client.upload_from_path(upload_file)
        hash = res["link"]
    except Exception as err:
        print("upload_image err : {}".format(err))

    return hash