from selenium import webdriver
from crawl_url import final_crawling
from screenshot_website import screenshot_integration
from upload_file_imgur import upload_image,writeTextFile
from time import sleep
import requests
import pandas as pd
import os

from selenium.webdriver.chrome.options import Options


def read_xlsx():
    retList = [],[]
    try:

        filename = "resources/details.xlsx"
        data = pd.read_excel(filename)
        df = pd.DataFrame(data)

        xlxs_instance = df.from_records(data)

        tmp = []
        url_list = []

        count = 0
        for i in xlxs_instance.get("URL"):
            url = i
            username =  xlxs_instance.get("Username")[count]
            password =  xlxs_instance.get("Password")[count]
            wp_admin =  xlxs_instance.get("wp_admin")[count]

            data = [url,username,password,wp_admin]
            tmp.append(data)
            url_list.append(url)
            count+=1

        retList = tmp,url_list

    except Exception as err:
        print(err)
    return retList


def login_website(dataList, driver):
    retTF = False

    _ , user_str, pass_str, login_url = dataList
    try:
        print("ACCESSING  : {}".format(login_url))
        try:
            requests.get(login_url, timeout=5)
        except requests.exceptions.Timeout as e:
            print("WEBSITE IS DOWN")
            return retTF

        driver.get(login_url)

        username = driver.find_element_by_id("user_login")
        username.clear()
        username.send_keys(user_str)

        password = driver.find_element_by_name("pwd")
        password.clear()
        password.send_keys(pass_str)

        driver.find_element_by_name("rememberme").click()
        driver.find_element_by_name("wp-submit").click()

        try:
            driver.find_element_by_id('login_error')
        except Exception as _:
            retTF = True

    except Exception as err:
        print("login_website err: {}".format(err))

    return retTF



def seessionLogin():

    dataList, url_list = read_xlsx()

    chrome_options = Options()
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option('useAutomationExtension', False)

    driver = webdriver.Chrome(chrome_options=chrome_options)

    feedback = []
    for i in dataList:
        islogin = login_website(i,driver)
        url = i[0]

        if islogin:
            print("SUCCESS LOGIN")
            upload_file = screenshot_integration(driver, url)

            if os.path.exists(upload_file):
                hashlink = upload_image(upload_file)
                feedback.append("SUCCESS : {}".format(hashlink))

            else:
                feedback.append("ERROR SCREENSHOT PROCESS : {}".format(url))

        else:
            feedback.append("LOGIN FAILED : {}".format(url))
            print("WRONG USERNAME OR PASSWORD!")

        print()


    driver.close()
    driver.quit()


    writeTextFile(feedback)



if __name__ == "__main__":
    seessionLogin()

