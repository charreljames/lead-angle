
from bs4 import BeautifulSoup
import requests
import re

def crawl_url(url):
    return_link_list = []
    page = requests.get(url)
    soup = BeautifulSoup(page.text, "html.parser")
    articles = soup.findAll('article')

    for article in articles:
        paragraph_list = article.findAll("p")
        if paragraph_list:
            for paragraph in paragraph_list:
                linkList = paragraph.findAll("a")
                if linkList:
                    for link in linkList:
                        return_link_list.append(link.text)

    return return_link_list

def final_crawling(url):
    tmp = []
    try:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, "html.parser")
        paragraph_list = soup.findAll('p')
        if paragraph_list:
            for paragraph in paragraph_list:
                linkList = paragraph.findAll("a")
                if linkList:
                    # we remove the unnecessary link
                    keys = linkList[0].attrs
                    if len(keys) == 1:
                        # print(linkList)
                        tmp.append(linkList[0].text)

    except Exception as err:
        print("final_crawling err: {}".format(err))

    return tmp


if __name__ == "__main__":
    url_list = [
        "http://stubbsthezombie.com/nisan-motors-a-complete-overview-of-incredible-car-manufacturer-company/",
        "https://savebats.org/what-is-nissan-leaf-is-it-a-reliable-car-or-not/",
        "https://so-compa.com/a-sneak-peek-into-the-future-prospects-of-volkswagen/",
        "https://spunkysprout.com/how-maintaining-modern-volkswagen-vehicles-has-become-more-complex/",
        "http://hashtaggedpodcast.com/diving-deep-into-the-rich-history-of-the-holden-cars/",
        "https://jagermeistermusictour.com/the-modern-car-collection-of-holden-and-its-features/"
    ]

    for url in url_list:
        linkTextList = final_crawling(url)
        print(linkTextList)
