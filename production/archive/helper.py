from selenium import webdriver
from bs4 import BeautifulSoup
from imgurpython import ImgurClient
from PIL import Image
import pandas as pd
from time import sleep
import random
import requests
from threading import Thread

image_links = []


def read_xlsx():
    retList = [],[]
    try:

        filename = "details.xlsx"
        data = pd.read_excel(filename)
        df = pd.DataFrame(data)

        xlxs_instance = df.from_records(data)

        tmp = []
        url_list = []

        count = 0
        for i in xlxs_instance.get("URL"):
            url = i
            username =  xlxs_instance.get("Username")[count]
            password =  xlxs_instance.get("Password")[count]
            wp_admin =  xlxs_instance.get("wp_admin")[count]

            data = [url,username,password,wp_admin]
            tmp.append(data)
            url_list.append(url)
            count+=1

        retList = tmp,url_list

    except Exception as err:
        print(err)
    return retList

def crawl_url(url):
    return_link_list = []
    page = requests.get(url)
    soup = BeautifulSoup(page.text, "html.parser")
    articles = soup.findAll('article')

    for article in articles:
        paragraph_list = article.findAll("p")
        if paragraph_list:
            for paragraph in paragraph_list:
                linkList = paragraph.findAll("a")
                if linkList:
                    for link in linkList:
                        return_link_list.append(link.text)

    return return_link_list


def screenshot_special_link(url, linkTextList):
    return_list = []
    driver = webdriver.Chrome()
    driver.get(url)

    if linkTextList:
        for text in linkTextList:
            elements = driver.find_element_by_link_text(text)
            location = elements.location

            x = location["x"]
            y = location["y"]
            id = random.randrange(1000, 9000)
            filename = "images/{}.png".format(id)
            elements.screenshot(filename)
            return_list.append([filename, x, y])

            # ele = driver.find_element_by_xpath('//a[text()="'+text+'"]')
            # driver.execute_script('arguments[0].setAttribute("style", "color: rgba(0, 0, 0, 0);")', ele)

    return return_list


def screenshot_website_images(url,textlist):
    options = webdriver.ChromeOptions()
    options.headless = True

    driver = webdriver.Chrome(options=options)
    driver.get(url)

    for text in textlist:
        ele = driver.find_element_by_xpath('//a[text()="' + text + '"]')
        driver.execute_script('arguments[0].setAttribute("style", "color: rgba(0, 0, 0, 0);")', ele)

    script = 'document.getElementsByTagName("body")[0].style.filter="blur(7px)"'
    driver.execute_script(script)

    web_image_id = random.randrange(1000, 9000)
    images_background = 'images/{}.png'.format(web_image_id)

    S = lambda X: driver.execute_script('return document.body.parentNode.scroll' + X)
    driver.set_window_size(S('Width'), S('Height'))  # May need manual adjustment
    driver.find_element_by_tag_name('body').screenshot(images_background)
    driver.quit()

    return images_background


def writeTextFile(textlist):
    with open("link.txt", "w") as f:
        for i in textlist:
            f.write(i + "\n")
        f.close()


def mainTask( url ):
    retTF = False
    try:

        client_id = "9825d47ce5d48e0"
        client_secret = "5ac1a7913d736efb0fabc54749060e34faec942d"
        client = ImgurClient(client_id, client_secret)

        response_hash_list = []

        link_text_list = crawl_url(url)  # a text of the special link in the url

        print("HighLight Text  : " + str(link_text_list))
        print()

        if link_text_list:

            website_special_link = screenshot_special_link(url, link_text_list)

            website_background = screenshot_website_images(url, link_text_list)

            mainbackground = Image.open(website_background)

            if website_special_link:
                for i in website_special_link:
                    attach_image, x, y = i
                    area = (x, y)
                    tmp = Image.open(attach_image)
                    mainbackground.paste(tmp, area)

                upload_id = random.randrange(1000, 9000)
                upload_filename = "upload/{}.png".format(upload_id)
                mainbackground.save(upload_filename)

            #     res = client.upload_from_path(upload_filename)
            #     response_hash_list.append(res["link"])
            #
            # if response_hash_list:
            #     writeTextFile(response_hash_list)

        retTF = True

    except Exception as err:
        print("mainTask Err: {}".format(err))

    return retTF



if __name__ == "__main__":
    mainTask()

    # res = crawl_url("http://stubbsthezombie.com/nisan-motors-a-complete-overview-of-incredible-car-manufacturer-company/")
    # print(res)