
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import random
from time import sleep

def capture_image_text(url, linkTextList):
    return_list = []

    chrome_options = Options()
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option('useAutomationExtension', False)

    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get(url)

    # elements = driver.find_element_by_link_text(text)
    elements = driver.find_element_by_class_name("entry-content")

    # driver.execute_script("return document.body.clientWidth")
    # driver.execute_script("return window.innerHeight")

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    location = elements.location
    x = location["x"]
    y = location["y"]

    id = random.randrange(1000, 9000)
    filename = "images/{}.png".format(id)
    elements.screenshot(filename)


    # if linkTextList:
    #     for text in linkTextList:
    #         elements = driver.find_element_by_link_text(text)
    #
    #         driver.execute_script("return document.body.clientWidth")
    #         driver.execute_script("return window.innerHeight")
    #
    #         location = elements.location
    #         x = location["x"]
    #         y = location["y"]
    #         id = random.randrange(1000, 9000)
    #         filename = "images/{}.png".format(id)
    #         elements.screenshot(filename)


    driver.quit()
    return return_list


if __name__ == "__main__":
    url = "http://stubbsthezombie.com/nisan-motors-a-complete-overview-of-incredible-car-manufacturer-company/"
    linkTextList = ['Nissan dismantlers']
    capture_image_text(url, linkTextList)
