import os
import time

from PIL import Image
from selenium import webdriver
import random

def fullpage_screenshot(driver, file):

        # script = 'document.getElementsByTagName("body")[0].style.filter="blur(7px)"'
        # driver.execute_script(script)

        driver.maximize_window()

        try:
            element = driver.find_element_by_tag_name('nav')
            driver.execute_script("""
                    var element = arguments[0];
                    element.parentNode.removeChild(element);
                    """, element)
        except Exception as _:
            pass


        total_width = driver.execute_script("return document.body.offsetWidth")
        total_height = driver.execute_script("return document.documentElement.scrollHeight")

        viewport_width = driver.execute_script("return document.body.clientWidth")
        viewport_height = driver.execute_script("return window.innerHeight")
        rectangles = []

        i = 0
        while i < total_height:
            ii = 0
            top_height = i + viewport_height

            if top_height > total_height:
                top_height = total_height

            while ii < total_width:
                top_width = ii + viewport_width

                if top_width > total_width:
                    top_width = total_width
                rectangles.append((ii, i, top_width,top_height))

                ii = ii + viewport_width

            i = i + viewport_height

        stitched_image = Image.new('RGB', (total_width, total_height))
        previous = None
        part = 0

        for rectangle in rectangles:
            if not previous is None:
                driver.execute_script("window.scrollTo({0}, {1})".format(rectangle[0], rectangle[1]))
                time.sleep(0.2)

            file_name = "part_{0}.png".format(part)
            driver.get_screenshot_as_file(file_name)
            screenshot = Image.open(file_name)

            if rectangle[1] + viewport_height > total_height:
                offset = (rectangle[0], total_height - viewport_height)
            else:
                offset = (rectangle[0], rectangle[1])

            stitched_image.paste(screenshot, offset)

            del screenshot
            os.remove(file_name)
            part = part + 1
            previous = rectangle

        stitched_image.save(file)
        print("Finishing chrome full page screenshot workaround...")
        return True


def capture_web(url):
    from selenium.webdriver.chrome.options import Options

    chrome_options = Options()
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option('useAutomationExtension', False)

    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get(url)
    image_id = random.randrange(1000,9000)

    filename =  "images/{}.png".format(image_id)
    fullpage_screenshot(driver,filename)

    linkTextList = ['Nissan dismantlers']

    if linkTextList:
        for text in linkTextList:
            elements = driver.find_element_by_link_text(text)

            location = elements.location
            size = elements.size
            x = location["x"]
            y = location["y"]

            height = size["height"]
            width = size["width"]






    driver.quit()


if __name__ == "__main__":
    # urls = [
    #     "http://stubbsthezombie.com/nisan-motors-a-complete-overview-of-incredible-car-manufacturer-company/",
    #     "https://savebats.org/what-is-nissan-leaf-is-it-a-reliable-car-or-not/",
    #     "https://so-compa.com/a-sneak-peek-into-the-future-prospects-of-volkswagen/"
    # ]
    # for url in urls:
    #         capture_web(url)

    capture_web("http://stubbsthezombie.com/nisan-motors-a-complete-overview-of-incredible-car-manufacturer-company/")