
from bs4 import BeautifulSoup
import requests

def final_crawling_old(url):
    tmp = []
    try:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, "html.parser")
        paragraph_list = soup.findAll('p')

        if paragraph_list:
            for paragraph in paragraph_list:
                linkList = paragraph.findAll("a")
                if linkList:
                    # we remove the unnecessary link
                    keys = linkList[0].attrs
                    if len(keys) == 1:
                        # print(linkList)
                        tmp.append(linkList[0].text)

    except Exception as err:
        print("final_crawling err: {}".format(err))

    return tmp


def final_crawling(source):
    tmp = []
    try:
        soup = BeautifulSoup(source, "html.parser")
        paragraph_list = soup.findAll('p')

        if paragraph_list:
            for paragraph in paragraph_list:
                linkList = paragraph.findAll("a")
                if linkList:
                    # we remove the unnecessary link
                    keys = linkList[0].attrs
                    if len(keys) == 1:
                        # print(linkList)
                        tmp.append(linkList[0].text)

    except Exception as err:
        print("final_crawling err: {}".format(err))

    return tmp



if __name__ == "__main__":

    url_list = [
        "https://savebats.org/some-of-the-interesting-facts-about-cbd-oil-which-we-are-unaware-of/",
        "https://kaine2005.org/2019/11/13/brighter-side-of-cdb-oil-and-steps-to-use-it/",
        "https://californiaopencarry.org/2019/11/13/steps-to-follow-if-you-want-to-make-diy-body-butter-with-the-help-of-cbd-oil/",
        "https://activateinstruction.org/all-you-need-to-know-about-cbd-oil-and-its-working/",
        "https://sayitaintsoalready.com/reasons-why-cbd-oil-is-preferred-for-grapplers/",
        "https://complusalliance.org/what-are-the-crucial-benefits-associated-with-cbd-oil/",
        "http://sbimarathon.com/2019/11/15/have-you-used-cbd-oils-for-dogs-check-out-the-benefits/"
    ]

    for url in url_list:
        linkTextList = final_crawling(url)
