from selenium import webdriver
from time import sleep
import pandas as pd
from scrape import main_scrape
from proxifier import get_proxies
import random

class WebScrape():

    def __init__(self , keyword , number_domains):
        self.url_net = "https://www.expireddomains.net/"
        self.keyword = keyword
        self.NUMBER_WEBSITE = number_domains

        ipList = ["195.154.255.118:15001", "69.30.240.226:15001"]
        options = webdriver.ChromeOptions()
        options.add_argument('--proxy-server=http://%s' % random.choice(ipList))
        options.add_argument('headless')
        options.add_argument('window-size=1920x1080')
        options.add_argument("disable-gpu")
        self.driver = webdriver.Chrome(chrome_options=options)


    def get_website_acr(self):
        domain_acr = []
        try:
            table = self.driver.find_element_by_tag_name("table")
            df = pd.read_html(table.get_attribute('outerHTML'))

            if df:
                dataframe = df[0]
                index = 0

                for domain in dataframe.get("Domain"):
                    acr = dataframe.get("ACR")[index]
                    domain_acr.append([domain, acr])
                    index += 1

        except Exception as _:
            print("You have been block!")

        return domain_acr

    def retrieve_filter_website(self, data):
        desc = sorted(data, key=lambda x: x[1], reverse=True)
        webnameList = [i[0] for i in desc]

        if webnameList:
            for url in webnameList:
                main_scrape(self.driver, webnameList)
                print(url)


    def mainTask(self):

        print("STARTING PROCESS .....")
        sleep(15)

        self.driver.get(self.url_net)

        sleep(10)

        searchtext = self.driver.find_element_by_id("navsearchinput")
        searchtext.clear()
        searchtext.send_keys(self.keyword)

        self.driver.find_element_by_class_name("btn").click()

        sleep(10)

        desc = []
        page = 0

        while True:
            domain_acr = self.get_website_acr()

            if not domain_acr: break

            desc+=domain_acr

            sleep(10)

            if page > self.NUMBER_WEBSITE:
                break

            self.driver.find_element_by_link_text("Next Page »").click()
            sleep(10)

            page+=25

        self.driver.quit()

        newdesc = sorted(desc, key=lambda x: x[1], reverse=True)
        # webnameList = [i[0] for i in newdesc]

        print("GENERATED DOMAINS : {}".format(newdesc))

        start_year = 2009
        end_year = 2019

        for datalist in newdesc:
            webname, _ = datalist
            for year in range(start_year, end_year + 1):
                main_scrape(self.driver, year, webname)

        print("TRANSACTION DONE")


if __name__ == "__main__":
    keyword = "health"
    ins = WebScrape(keyword,25)
    ins.mainTask()