
# from scrape import main_scrape
#
# def foo(data):
#     desc = sorted(data, key=lambda x: x[1], reverse=True)
#     webnameList = [i[0] for i in desc]
#
#     print(webnameList)
#
#
# if __name__ == "__main__":
#     data = [['Health.sc', 17], ['Health.gd', 5], ['Health.rw', 7], ['Health.co', 11], ['Health.com.co', 2], ['Health.ac', 24], ['Health.energy', 6], ['Health.fitness', 14], ['Health.org.es', 3], ['Health.co.at', 9], ['Health.kg', 21], ['Health.re', 23], ['Health.watch', 7], ['Health.kitchen', 6], ['Health.hospital', 1], ['Health.gifts', 1], ['Health.clinic', 2], ['Health.club', 8], ['Health.ca', 557], ['Health.fr', 53], ['Health.place', 6], ['Health.hiv', 0], ['Health.link', 7], ['Health.party', 1], ['Health.cab', 2], ['Health.bike', 0], ['Health.rent', 0], ['Health.team', 11], ['Health.news', 1342], ['Health.tel', 7], ['Health.guru', 17], ['Health.technology', 1], ['Health.tips', 17], ['Health.email', 1], ['Health.network', 6], ['Health.brussels', 2], ['Health.finance', 12], ['Health.cologne', 1], ['Health.partners', 1], ['Health.community', 1], ['Health.wang', 1], ['Health.top', 0], ['Health.uno', 0], ['Health.rich', 0], ['Health.onl', 0], ['Health.markets', 0], ['Health.stream', 0], ['Health.cc', 66], ['Health.kim', 1], ['Health.bid', 0], ['Health.trade', 0], ['AssociatedHealth.com', 51], ['miahealth.mk', 12], ['HealthierLife.gi', 21], ['HealthDvd.as', 38], ['HealthyLiving.ky', 2], ['Health.bz', 14], ['healthnettpo.bi', 11], ['HealthMatters.tc', 5], ['Healthcare.cr', 7], ['Health.equipment', 1], ['SmartHealth.uy', 0], ['Health.fashion', 1], ['Health.wales', 2], ['HealthFood.to', 0], ['HealthMedia.uz', 18], ['DigiHealth.ug', 0], ['Healthy.cx', 0], ['NaturalHealthResearch.ac', 0], ['HealthyLiving.aw', 27], ['ArmHealth.am', 185], ['HealthInsurance.li', 22], ['HealthyPeople.li', 16], ['HealthNow.am', 35], ['HealthLaw.gs', 0]]
#     foo(data)

from time import sleep
import requests
from selenium import webdriver
from scrape import scrape_web,scrape_html


def main_scrape(year,domain, webname):
    try:
        api = "https://web.archive.org/web/{}*/{}".format(year,domain)
        driver = webdriver.Chrome()
        driver.get(api)

        sleep(10)
        container = driver.find_element_by_class_name("calendar-grid")
        linkList = []
        if container:
            try:
                link_element = container.find_elements_by_tag_name("a")
                for el in link_element:
                    try:
                        linkList.append(el.get_attribute("href"))
                    except Exception as _:
                        pass

            except Exception as _:
                print("No Screenshot")

        else:
            print("NO TABLE EXISTED!")

        if linkList:
            linkList = list(set(linkList))
            myArticleList = []

            for link in linkList:
                articleList = scrape_web(driver,link)
                if articleList:
                    myArticleList+=articleList

            print("PROCEED ON SCRAPING THE ARTICLE")
            scrape_html(driver, webname, myArticleList)



    except Exception as err:
        print("Main Scrape Error : {}".format(err))


if __name__ == "__main__":
    year = 2018
    domain = "Health.news"
    main_scrape(year, domain)

    print()

