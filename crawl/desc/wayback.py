from time import sleep
from langdetect import detect

def get_calendar(driver, year, webname):
    linkList = []
    try:
        api = "https://web.archive.org/web/{}*/{}".format(year, webname)
        driver.get(api)

        sleep(10)
        container = driver.find_element_by_class_name("calendar-grid")
        if container:
            bydayList = container.find_elements_by_class_name("month-day-container")

            for day in bydayList:
                try:
                    indicator = day.find_element_by_class_name("position")
                    if indicator:
                        try:
                            indicator.find_element_by_class_name("s3xx")
                        except Exception as _:
                            links = day.find_elements_by_tag_name("a")
                            for link in links:
                                tmp = link.get_attribute("href")
                                linkList.append(tmp)

                except Exception as _:
                    pass

    except Exception as _:
        print("{} WAYBACK CALENDAR IS NOT ACCESSABLE".format(webname))

    return linkList


def identify_english_website(content):
    retTF = False
    try:
        retTF = True
    except Exception as error:
        print("identify_english_website : {}".format(error))

    return retTF


if __name__ == "__main__":
    identify_english_website()
