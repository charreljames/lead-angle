import os
import random


def readTextFile():
    with open("weblist.txt", "r") as file:
        ipList = file.readlines()
    tmpList = [i.rstrip("\n") for i in ipList]
    return tmpList


def writeFile( dir, count_word, url, title, content ):


    # if count_words is greater than or equal to 300
    filename = "{}/{}_data.txt".format(dir,count_word)

    if os.path.exists(filename):
        filename = "{}/{}_({})_data.txt".format(dir, count_word,random.randrange(1000,5000))

    source = "Sources : {}".format(url)
    print("CONTENT WRITING : {}".format(filename))

    with open(filename, "w") as file:
        file.write("Title: {} ".format(str(title)) + "\n\n")
        file.write(source +"\n\n\n")

        file.write("#### ARTICLE #### \n")

        for paragraph in content:
            try:
                file.write(str(paragraph)+"\n\n")
            except Exception as _:
                pass

        file.close()

