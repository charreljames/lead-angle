from bs4 import BeautifulSoup
from time import sleep
import random
import os
import requests
from selenium import webdriver
import global_variable as GV
from wayback import get_calendar,identify_english_website
from helper_file import readTextFile,writeFile


WordCount = 300


# scrape the html and write in textfile
def scrape_html(driver, webname , url):
    try:

        dir = "text_output/"+webname
        try:
            driver.set_page_load_timeout(50)
            driver.get(url)

        except Exception as _:
            driver.execute_script("window.stop();")
            sleep(2)

        soup = BeautifulSoup(driver.page_source, "html.parser")
        try:
            title = soup.find("title").text
        except Exception as _:
            title = ""

        if title != "" and title not in GV.titlename: # filter title

            divList = soup.findAll("div")
            tmp_content = []
            tmp_words = 0

            for div in divList:
                try:
                    paragraphList = div.findAll("p")
                    for paragraph in paragraphList:
                        # tmp_Str = ""
                        if paragraph.findChild() == None:
                            tmp_Str = paragraph.text
                        else:
                            tmp_Str = str(paragraph.text).replace(str(paragraph.findChild()), str(paragraph.findChild().text))

                        if tmp_Str not in tmp_content:
                            tmp_content.append(tmp_Str)
                            tmp_words += len(tmp_Str.split(" "))

                except Exception as _:
                    pass

            if tmp_words >= WordCount:
                if tmp_content not in GV.article_content: # filter same content
                    writeFile(dir, tmp_words, url, title, tmp_content)
                    GV.article_content.append(tmp_content)

            else:
                print("CONTENT IS BELOW MINIMUM WORDS : {}".format(url))

            GV.titlename.append(title)

    except Exception as _:
        print("SORRY WEB DRIVER CANT ACCESS {}".format(url))


def archive_scrape( url_list , webname ):

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(options=options)

    try:
        for url in url_list:
            try:
                driver.set_page_load_timeout(50)
                driver.get(url)
            except Exception as _:
                driver.execute_script("window.stop();")


            webtitle = driver.title

            if webtitle == "":
                print("{} ACCESS DENIED!")
                return

            # if not identify_english_website(webtitle):
            #     print("{} WEBSITE IS NOT AN ENGLISH".format(url))
            #     return

            aList = driver.find_elements_by_tag_name("a")

            for atag in aList:
                source = str(atag.get_attribute("href"))

                if source.endswith(webname+"/"):
                    continue


                if webname in source:
                    excluded_words = ["index","feed","privacy","sitemap","about"]
                    isvalid = True
                    for word in excluded_words:
                        if word in source:
                            isvalid = False
                            break

                    if isvalid:

                        tmpList = []
                        if "html" in source: # for sure this a article
                            tmpList.append(source)

                        else: # a homepage or article
                            r = requests.get(source, timeout = 10)
                            if r.status_code == 200:
                                soup = BeautifulSoup(r.text, "html.parser")
                                levl2Link = soup.findAll(href=True)

                                for i in levl2Link:
                                    href_value =  i.get('href')
                                    if "html" in href_value:
                                        tmpList.append(href_value)

                        for i in tmpList:
                            if i.startswith("http"):
                                tmp = "http" + i.split("http")[-1]
                                if tmp not in GV.ARCHIVE_URL.keys():
                                    scrape_html(driver, webname, i)

                                    GV.ARCHIVE_URL[tmp] = i


    except Exception as archive_scrape_error:
        print("ARCHIVE SCRAPE : {}".format(archive_scrape_error))

    return


def main_scrape(driver, year, webname):
    retTF = False
    try:
        dir = "text_output/" + webname
        if not os.path.exists(dir):
            os.mkdir(dir)

        linkList = get_calendar(driver, year, webname)
        return linkList

    except Exception as _:
        print("Main Scrape Error, THIS IS NOT ACCESSABLE : {}".format(webname))

    return retTF



if __name__ == "__main__":
    from multiprocessing import Process

    # todo: scape those ulr that not ends with .html
    # by doing that we need to see if  there is an web.org link exist in that page
    # if yes then skip
    # if no then count it as part of scraping


    ipList = ["195.154.255.118:15001","69.30.240.226:15001"]
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    # options.add_argument('--proxy-server=http://%s' % random.choice(ipList))


    driver = webdriver.Chrome(options=options)

    webList = readTextFile()
    start_year = 2010
    end_year = 2011

    data_dict = {}

    for webname in webList:
        toscrape = []
        for year in range(start_year, end_year):
            print("SCRAPING {} IN YEAR {}".format(webname, year))
            webpages = main_scrape(driver, year, webname)
            toscrape+=webpages
        data_dict[webname] = toscrape
        print()
    driver.quit()



    processes = []
    for i in data_dict:
        url_list = data_dict[i]
        webname = i

        p = Process(target=archive_scrape, args=(url_list, webname,))
        p.start()
        processes.append(p)

    for pro in processes:
        pro.join()





        # print("PROCEED ON SCRAPING THE ARTICLES OF {} IN YEAR {} - {}".format(webname, start_year,end_year))
        # print("ARTICLE LIST : {}".format(GV.ARCHIVE_URL))
        # urList = [GV.ARCHIVE_URL[dd] for dd in GV.ARCHIVE_URL]
        # scrape_html(driver, webname, urList)
        #
        #
        # GV.titlename = []
        # GV.article_content = []
        # GV.ARCHIVE_URL = {}
        #
        # print()



