from selenium import webdriver
from lxml.html import fromstring
from time import sleep
import requests

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)
    return proxies

print(get_proxies())

# driver = webdriver.Chrome()
#
# proxies = get_proxies(driver)
#
# driver.close()
# for proxy in proxies:
#     PROXY = proxy
#     options = webdriver.ChromeOptions()
#     options.add_argument("--start-maximized")
#     options.add_argument('--proxy-server=http://%s' % PROXY)
#     driver = webdriver.Chrome(options=options)
#     url = "https://whatismyipaddress.com/"
#
#     driver.set_page_load_timeout(10)
#     driver.get(url)
#     sleep(10)
#     # Mostly free proxies will get proxy server errors.
#
#
#     sleep(50)
#     driver.close()













# 69.30.240.226:15001
# 195.154.255.118:15001