
from bs4 import BeautifulSoup
from selenium import webdriver
from time import  sleep
import requests


def archive_scrape(url,web_org):
    resList = []
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    linkList = soup.findAll('a')

    if linkList:

        for link in linkList:
            url = link.attrs["href"]

            if url.startswith(web_org):
                if url not in resList:
                    resList.append(url)

    return resList



def scrape_paragraph_tag(url):

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1920x1080')
    options.add_argument("disable-gpu")
    driver = webdriver.Chrome(chrome_options=options)

    driver.get(url)

    soup = BeautifulSoup(driver.page_source, "html.parser")
    divList = soup.findAll("div")

    tmp_content = []
    tmp_words = 0

    for div in divList:
        try:

            paragraphList = div.findAll("p")
            for paragraph in paragraphList:

                # tags = ["<b>","<strong>","<i>","<em>","<mark>","<small>","<del>","<ins>","<sub>","<sup>"]

                tmp_Str = ""
                if paragraph.findChild() == None:
                    tmp_Str = paragraph.text
                else:
                    # print(paragraph.findChild())
                    #
                    # if paragraph.findChild().text == "":
                    #     tmp_Str = paragraph.text

                    tmp_Str = str(paragraph.text).replace(str(paragraph.findChild()), str(paragraph.findChild().text))

                if tmp_Str not in tmp_content:
                    tmp_content.append(tmp_Str)
                    tmp_words+= len(tmp_Str.split(" "))


        except Exception as scrape_paragraph_tag_err:
            print("scrape_paragraph_tag : {}".format(scrape_paragraph_tag_err))


    for i in tmp_content:
        print(i+"\n")
    #
    # print(tmp_words)



def testFlashPlayer():
    url = "https://web.archive.org/web/20100604/stargroup1.com"

    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options

    options = Options()

    options.add_argument("--disable-features=EnableEphemeralFlashPermission");

    driver = webdriver.Chrome(options=options)

    driver.get(url)
    sleep(20)



def test_scrape():
    from scrape import scrape_html

    driver = webdriver.Chrome()
    data = ['https://web.archive.org/web/20090612/yeastinfectionsremedies.org', 'https://web.archive.org/web/20090715/yeastinfectionsremedies.org', 'https://web.archive.org/web/20090728/yeastinfectionsremedies.org', 'https://web.archive.org/web/20090823/yeastinfectionsremedies.org', 'https://web.archive.org/web/20090927/yeastinfectionsremedies.org', 'https://web.archive.org/web/20091228/yeastinfectionsremedies.org']
    webname = "yeastinfectionsremedies.org"
    scrape_html(driver, webname, data)



def beautifulScrape():
    url = "https://web.archive.org/web/20090812235455/http://www.yeastinfectionsremedies.org/vaginal-yeast-infections.html"

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(options=options)

    driver.get(url)
    soup = BeautifulSoup(driver.page_source, "html.parser")

    divList = soup.findAll("div")

    for div in divList:
        paragraphList = div.findAll("p")
        for paragraph in paragraphList:

            if paragraph.findChild() == None:
                tmp_Str = paragraph.text
            else:
                tmp_Str = str(paragraph.text).replace(str(paragraph.findChild()), "")


if __name__ == "__main__":
    beautifulScrape()

    # url = "https://web.archive.org/web/20160125203934/http://www.fairloanrate.com/2015/07/08/car-purchase-made-easy-thanks-to-loan-pre-approvals/"
    # web_org = "https://web.archive.org/web/20160125203934/http://www.fairloanrate.com"
    # # archive_scrape(url,web_org)
    #
    # urlList = [
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9164-mercedes-benz-c200-cgi.html",
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9174-2010-bmw-x5.html",
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9185-freightliner-is-now-set-to-deliver-their-assets-to-discovery-channel.html",
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9179-are-you-planning-for-gm-vehicle.html",
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9194-cruise-with-chevrolet-cruze.html",
    #     "https://web.archive.org/web/20100822163958/http://www.autolinemag.com/959-toyota-fr-s.html",
    #     "https://web.archive.org/web/20101014152628/http://www.autolinemag.com/9146-gm-volt-electric-car.html",
    #     "https://web.archive.org/web/20100822163958/http://www.autolinemag.com/956-rumors-toyota-ft-86-concept-to-be-named.html",
    #     "https://web.archive.org/web/20100822163958/http://www.autolinemag.com/962-rumors-toyota.html"
    # ]
    #
    # scrape_paragraph_tag("https://web.archive.org/web/20101229024732/http://www.autolinemag.com/575-ford-figo-review.html")