from bs4 import BeautifulSoup
import random
import requests
from time import sleep
import os
from selenium import webdriver

# scrape the html and write in textfile
def scrape_html(driver, webname, article_links_list):
    try:

        index = 0
        dir = "text_output/"+webname
        if article_links_list:
            article_list = list(set(article_links_list))
            print("ARTICLE LINKS : {}".format(article_list))

            title = ""
            content = []
            count_word = 0

            for url in article_list:
                driver.get(url)
                try:
                    title = driver.find_element_by_tag_name("h1").text
                except Exception as _:
                    pass

                try:
                    paragraphList = driver.find_elements_by_tag_name("p")
                    for paragraph in paragraphList:
                        content.append(paragraph.text)
                        count_word+=len(paragraph.text)

                except Exception as _:
                    pass

                if count_word >= 300 and title != "":
                    filename = "{}/{}_data.txt".format(dir,count_word)

                    if os.path.exists(filename):
                        ran_id = random.randrange(1000,9000)
                        filename = "{}/{}_({})_data.txt".format(dir,count_word,ran_id)

                    print("WRITING : {}".format(filename))

                    with open(filename, "w") as file:
                        file.write("##### Title #####\n")
                        file.write(title + "\n\n\n")

                        file.write("##### ARTICLE #####")
                        for paragraph in content:
                            file.write(paragraph+"\n")
                        file.write("###################\n")

                    index+=1

        if index>0:
            print("SUCESSFULY SCRAPE")

    except Exception as err:
        print(err)


# returns a list of crawl website articles
def scrape_web(driver, website_archive_url):
    linkList = []
    try:
        # website_archive_url : https://web.archive.org/web/20180919/Health.news
        driver.get(website_archive_url)
        links = driver.find_elements_by_tag_name("a")
        for link in links:
            tmp = str(link.get_attribute("href"))
            if tmp.startswith("https://") and tmp.endswith("html"):
                if "index.html" in tmp:
                    pass
                elif "privacypolicy.html" in tmp:
                    pass
                else:
                    linkList.append(str(tmp))

    except Exception as err:
        print("Scrape Web Error : {}".format(err))

    return linkList


def main_scrape(driver, year, webname):
    retTF = False
    try:

        dir = "text_output/" + webname
        if not os.path.exists(dir):
            print("CREATING OUTPUT FOLDER : {}".format(dir))
            os.mkdir(dir)

        api = "https://web.archive.org/web/{}*/{}".format(year,webname)
        driver.get(api)

        sleep(10)
        container = driver.find_element_by_class_name("calendar-grid")
        linkList = []

        if container:
            try:
                link_element = container.find_elements_by_tag_name("a")
                for el in link_element:
                    linkList.append(el.get_attribute("href"))

            except Exception as _:
                print("No Archive History")
                return retTF
        else:
            print("NO CALENDAR VIEW")
            return retTF

        if linkList:
            print("CRAWL {} HISTORY".format(webname))
            myArticleList = []
            for link in linkList:

                print(link)
                articleList = scrape_web(driver,link)
                print("{} : {}".format(link,articleList))

                if articleList:
                    myArticleList+=articleList

            print("PROCEED ON SCRAPING THE ARTICLES OF {}".format(webname))
            scrape_html(driver, webname, myArticleList)

        else:
            print("{} DONT HAVE ANY ARTICLE IN YEAR {}".format(webname,year))
            return retTF

    except Exception as err:
        print("Main Scrape Error : {}".format(err))


    return retTF

if __name__ == "__main__":

    options = webdriver.ChromeOptions()
    # options.add_argument('headless')
    # options.add_argument('window-size=1920x1080')
    # options.add_argument("disable-gpu")
    driver = webdriver.Chrome(chrome_options=options)

    # website = ['Health.news', 'Health.ca', 'ArmHealth.am', 'Health.cc', 'Health.fr', 'AssociatedHealth.com', 'HealthDvd.as', 'HealthNow.am', 'HealthyLiving.aw', 'Health.ac', 'Health.re', 'HealthInsurance.li', 'Health.kg', 'HealthierLife.gi', 'HealthMedia.uz', 'Health.sc', 'Health.guru', 'Health.tips', 'HealthyPeople.li', 'Health.fitness', 'Health.bz', 'Health.finance', 'miahealth.mk', 'Health.co', 'Health.team', 'healthnettpo.bi', 'Health.co.at', 'Health.club', 'Health.rw', 'Health.watch', 'Health.link', 'Health.tel', 'Healthcare.cr', 'Health.energy', 'Health.kitchen', 'Health.place', 'Health.network', 'Health.gd', 'HealthMatters.tc', 'Health.org.es', 'Health.com.co', 'Health.clinic', 'Health.cab', 'Health.brussels', 'HealthyLiving.ky', 'Health.wales', 'Health.hospital', 'Health.gifts', 'Health.party', 'Health.technology', 'Health.email', 'Health.cologne', 'Health.partners', 'Health.community', 'Health.wang', 'Health.kim', 'Health.equipment', 'Health.fashion', 'Health.hiv', 'Health.bike', 'Health.rent', 'Health.top', 'Health.uno', 'Health.rich', 'Health.onl', 'Health.markets', 'Health.stream', 'Health.bid', 'Health.trade', 'SmartHealth.uy', 'HealthFood.to', 'DigiHealth.ug', 'Healthy.cx', 'NaturalHealthResearch.ac', 'HealthLaw.gs']

    webname = "Health.ca"
    year = 2018


    iscss = main_scrape(driver,year, webname)
    print(iscss)



