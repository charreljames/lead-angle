from bs4 import BeautifulSoup
import random
import requests
from time import sleep
import os
from selenium import webdriver

# scrape the html and write in textfile
def scrape_html(driver, webname, article_links_list):
    try:

        index = 0
        dir = "text_output/"+webname
        if article_links_list:
            article_list = list(set(article_links_list))
            print("ARTICLE LINKS : {}".format(article_list))

            title = ""
            content = []
            count_word = 0

            for url in article_list:
                driver.get(url)
                try:
                    title = driver.find_element_by_tag_name("h1").text
                except Exception as _:
                    pass

                try:
                    paragraphList = driver.find_elements_by_tag_name("p")
                    for paragraph in paragraphList:
                        content.append(paragraph.text)
                        count_word+=len(paragraph.text)

                except Exception as _:
                    pass

                if count_word >= 300 and title != "":
                    filename = "{}/{}_data.txt".format(dir,count_word)

                    if os.path.exists(filename):
                        ran_id = random.randrange(1000,9000)
                        filename = "{}/{}_({})_data.txt".format(dir,count_word,ran_id)

                    print("WRITING : {}".format(filename))

                    with open(filename, "w") as file:
                        file.write("##### Title #####\n")
                        file.write(title + "\n\n\n")

                        file.write("##### ARTICLE #####\n")
                        for paragraph in content:
                            file.write(paragraph+"\n")
                        file.write("###################\n")

                    index+=1

        if index>0:
            print("SUCESSFULY SCRAPE")

    except Exception as err:
        print(err)


def archive_scrape(url, webname):
    resList = []
    r = requests.get(url, timeout = 10)
    soup = BeautifulSoup(r.text, "html.parser")
    linkList = soup.findAll('a')
    if linkList:
        for link in linkList:
            if str(link).startswith("<a"):
                article_url = link.attrs["href"]

                notInWords = ["index","privacy","sitemap","about"]
                if webname in article_url :
                    for exclude_words in notInWords:
                        if exclude_words not in article_url:
                            if article_url not in resList:
                                resList.append(article_url)
    return resList


def main_scrape(driver, year, webname):
    retTF = False
    try:
        dir = "text_output/" + webname
        if not os.path.exists(dir):
            print("CREATING OUTPUT FOLDER : {}".format(dir))
            os.mkdir(dir)

        api = "https://web.archive.org/web/{}*/{}".format(year,webname)
        driver.get(api)

        sleep(10)
        container = driver.find_element_by_class_name("calendar-grid")
        linkList = []

        if container:
            try:
                link_element = container.find_elements_by_tag_name("a")
                for el in link_element:
                    linkList.append(el.get_attribute("href"))

            except Exception as _:
                print("No Archive History")
                return retTF

        else:
            print("NO CALENDAR VIEW")
            return retTF


        if linkList:
            print("CRAWL {} HISTORY".format(webname))
            myArticleList = []

            for link in linkList:
                articleList = archive_scrape(link,webname)
                if articleList:
                    print("{} : {}".format(link,articleList))
                else:
                    print("{} : History on Web Archive Not Found".format(link))

                if articleList:
                    myArticleList+=articleList

            print("PROCEED ON SCRAPING THE ARTICLES OF {}".format(webname))
            scrape_html(driver, webname, myArticleList)

        else:
            print("{} DONT HAVE ANY ARTICLE IN YEAR {}".format(webname,year))
            return retTF

    except Exception as err:
        print("Main Scrape Error : {}".format(err))


    return retTF

if __name__ == "__main__":

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1920x1080')
    options.add_argument("disable-gpu")
    driver = webdriver.Chrome(chrome_options=options)

    # website = ['Health.news', 'Health.ca', 'ArmHealth.am', 'Health.cc', 'Health.fr', 'AssociatedHealth.com', 'HealthDvd.as', 'HealthNow.am', 'HealthyLiving.aw', 'Health.ac', 'Health.re', 'HealthInsurance.li', 'Health.kg', 'HealthierLife.gi', 'HealthMedia.uz', 'Health.sc', 'Health.guru', 'Health.tips', 'HealthyPeople.li', 'Health.fitness', 'Health.bz', 'Health.finance', 'miahealth.mk', 'Health.co', 'Health.team', 'healthnettpo.bi', 'Health.co.at', 'Health.club', 'Health.rw', 'Health.watch', 'Health.link', 'Health.tel', 'Healthcare.cr', 'Health.energy', 'Health.kitchen', 'Health.place', 'Health.network', 'Health.gd', 'HealthMatters.tc', 'Health.org.es', 'Health.com.co', 'Health.clinic', 'Health.cab', 'Health.brussels', 'HealthyLiving.ky', 'Health.wales', 'Health.hospital', 'Health.gifts', 'Health.party', 'Health.technology', 'Health.email', 'Health.cologne', 'Health.partners', 'Health.community', 'Health.wang', 'Health.kim', 'Health.equipment', 'Health.fashion', 'Health.hiv', 'Health.bike', 'Health.rent', 'Health.top', 'Health.uno', 'Health.rich', 'Health.onl', 'Health.markets', 'Health.stream', 'Health.bid', 'Health.trade', 'SmartHealth.uy', 'HealthFood.to', 'DigiHealth.ug', 'Healthy.cx', 'NaturalHealthResearch.ac', 'HealthLaw.gs']
    # years = [2010,2011,2012,2013,2014,2015,2016,2017,2018,2019]

    webname = "fairloanrate.com"
    year = 2016
    main_scrape(driver,year, webname)

    # archive_scrape("https://web.archive.org/web/20160110/fairloanrate.com", "fairloanrate.com")
