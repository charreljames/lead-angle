
from bs4 import BeautifulSoup
import requests


def archive_scrape(url,web_org):
    resList = []
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    linkList = soup.findAll('a')

    if linkList:

        for link in linkList:
            url = link.attrs["href"]

            if url.startswith(web_org):
                if url not in resList:
                    resList.append(url)

    return resList

if __name__ == "__main__":
    url = "https://web.archive.org/web/20160125203934/http://www.fairloanrate.com/2015/07/08/car-purchase-made-easy-thanks-to-loan-pre-approvals/"
    web_org = "https://web.archive.org/web/20160125203934/http://www.fairloanrate.com"
    archive_scrape(url,web_org)