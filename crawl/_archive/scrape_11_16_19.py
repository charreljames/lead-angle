from bs4 import BeautifulSoup
import random
import requests
from time import sleep
import os
from selenium import webdriver
import global_variable as GV


# update this base on your ideal number of words
WordCount = 200

def writeFile( dir, count_word, url, title, content ):

    # if count_words is greater than or equal to 300
    filename = "{}/{}_data.txt".format(dir,count_word)

    if os.path.exists(filename):
        filename = "{}/{}_({})_data.txt".format(dir, count_word,random.randrange(1000,5000))

    source = "Sources : {}".format(url)
    print("CONTENT WRITING : {}".format(filename))

    with open(filename, "w") as file:
        file.write("Title: {} ".format(str(title)) + "\n\n")
        file.write(source +"\n\n\n")

        file.write("#### ARTICLE #### \n")

        for paragraph in content:
            try:
                file.write(str(paragraph)+"\n\n")
            except Exception as _:
                pass

        file.close()


# scrape the html and write in textfile
def scrape_html(driver, webname, article_links_list):
    try:

        dir = "text_output/"+webname
        if article_links_list:
            article_list = list(set(article_links_list))

            for url in article_list:
                try:
                    driver.set_page_load_timeout(10)
                    driver.get(url)
                    driver.execute_script("window.stop();")

                except Exception as _:
                    pass
                    # print("WEBSITE IS NOT ACCESSABLE OR BROKEN : {}".format(url))
                    # continue

                soup = BeautifulSoup(driver.page_source, "html.parser")
                try:
                    title = soup.find("title").text
                except Exception as _:
                    title = ""

                if title != "" and title not in GV.titlename: # filter title

                    divList = soup.findAll("div")
                    tmp_content = []
                    tmp_words = 0

                    for div in divList:
                        try:
                            paragraphList = div.findAll("p")
                            for paragraph in paragraphList:
                                # tmp_Str = ""
                                if paragraph.findChild() == None:
                                    tmp_Str = paragraph.text
                                else:
                                    tmp_Str = str(paragraph.text).replace(str(paragraph.findChild()), str(paragraph.findChild().text))

                                if tmp_Str not in tmp_content:
                                    tmp_content.append(tmp_Str)
                                    tmp_words += len(tmp_Str.split(" "))

                        except Exception as _:
                            pass

                    if tmp_words >= WordCount:
                        if tmp_content not in GV.article_content: # filter same content
                            writeFile(dir, tmp_words, url, title, tmp_content)
                            GV.article_content.append(tmp_content)

                    else:
                        print("CONTENT IS BELOW MINIMUM WORDS : {}".format(url))

                    GV.titlename.append(title)

        else:
            print("NO HISTORY ARTICLES")



    except Exception as err:
        print(err)


def archive_scrape(driver , url, webname):
    resList = []
    try:

        driver.set_page_load_timeout(40)
        driver.get(url)

        aList = driver.find_elements_by_tag_name("a")

        for atag in aList:
            source = atag.get_attribute("href")

            notInWords = ["index","privacy","sitemap","about"]
            if webname in source :
                for exclude_words in notInWords:
                    if exclude_words not in source and str(source).endswith("html"):
                        if source not in resList:
                            resList.append(source)

        # r = requests.get(url, timeout = 15)
        # soup = BeautifulSoup(r.text, "html.parser")
        # linkList = soup.findAll('a')
        # print(linkList)
        # if linkList:
        #     for link in linkList:
        #         if str(link).startswith("<a"):
        #             pass
                    # print(link)
                    # if "href" in link.attrs:
                    #     article_url = link.attrs["href"]
                    #
                    #     notInWords = ["index","privacy","sitemap","about"]
                    #     if webname in article_url :
                    #         for exclude_words in notInWords:
                    #             if exclude_words not in article_url and str(article_url).endswith("html"):
                    #                 if article_url not in resList:
                    #                     resList.append(article_url)
    except Exception as _:
        pass
    return resList


def main_scrape(driver, year, webname):
    retTF = False
    try:
        dir = "text_output/" + webname
        if not os.path.exists(dir):
            os.mkdir(dir)

        api = "https://web.archive.org/web/{}*/{}".format(year,webname)
        driver.get(api)

        sleep(5)
        container = driver.find_element_by_class_name("calendar-grid")
        linkList = []

        if container:
            try:
                link_element = container.find_elements_by_tag_name("a")
                for el in link_element:
                    tmp = el.get_attribute("href")
                    linkList.append(str(tmp))

            except Exception as _:
                return retTF

        else:
            return retTF

        if linkList:
            myArticleList = []
            for link in linkList:
                articleList = archive_scrape(driver,link,webname)

                if articleList:
                    myArticleList += articleList

                print("SCRAPING ...")

            print("PROCEED ON SCRAPING THE ARTICLES OF {} in : {} ".format(webname,year))
            print("ARTICLE LIST : {}".format(myArticleList))

            scrape_html(driver, webname, myArticleList)

    except Exception as _:
        print("Main Scrape Error, THIS IS NOT ACCESSABLE : {}".format(webname))


    return retTF


def readTextFile():
    with open("weblist.txt", "r") as file:
        ipList = file.readlines()
    tmpList = [i.rstrip("\n") for i in ipList]
    return tmpList



if __name__ == "__main__":

    #todo: website homepage is blocked because of flask

    options = webdriver.ChromeOptions()
    prefs = {
        "profile.default_content_setting_values.plugins": 1,
        "profile.content_settings.plugin_whitelist.adobe-flash-player": 1,
        "profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player": 1,
        "PluginsAllowedForUrls": "stargroup1.com"
    }

    options.add_experimental_option("prefs", prefs)

    # options.add_argument('headless')
    # options.add_argument('window-size=1920x1080')
    # options.add_argument("disable-gpu")
    driver = webdriver.Chrome(chrome_options=options)


    # webname = "fairloanrate.com"
    # year = 2010
    # main_scrape(driver, year, webname)

    webList = readTextFile()
    start_year = 2010
    end_year = 2019

    for webname in webList:
        for year in range(start_year, end_year + 1):
            main_scrape(driver, year, webname)

        GV.titlename = []
        GV.article_content = []
