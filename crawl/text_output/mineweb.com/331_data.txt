Title: FNX MINING COMPANY INC. | CompanyMine 

Sources : https://web.archive.org/web/20100104233716/http://www.infomine.com/index/companies/fnx_mining_company_inc..html


#### ARTICLE #### 

For more information visit...
www.fnxmining.com




FNX Mining Company Inc. (FNX : TSX) is a diversified Canadian mining company, which explores, develops and mines copper-nickel-platinum-palladium-gold in the prolific Sudbury mining camp.  FNX currently produces ore from three properties in the Sudbury Basin, Podolsky, Levack and McCreedy West, and ships the ore to Vale Inco's Sudbury facilities for processing under the terms of an offtake agreement. The Company produced approximately 700,000 tons of ore yielding 35 million pounds of copper, 5 million pounds of nickel and 58,000 ounces of precious metals in 2009.  FNX is forecasting a 30% increase in ore production for 2010 to approximately 890,000 tons producing 48 million pounds of copper, 8 million pounds of nickel and 75,000 ounces of precious metals.  Most of this increased production will come from the commencement of production in mid-2010 from the Company's high grade LFD. 




The FNX operating model has removed the fixed costs from mining, benefits from the pre-existing surface and underground infrastructure on the five former producing properties acquired in 2002 from Vale Inco and has no long term environmental liabilities.  The Company also benefits from the unique fact that in the Sudbury mining camp, there are distinctly separate nickel-rich and copper-precious metal-rich orebodies within the same mine.  This allows FNX to easily and quickly switch from a nickel focus to a copper-precious metal focus, dependent upon metal prices. Currently, the Company's mining operations produces predominately copper and precious metals, but can add nickel ore quickly to the mix. The Company suspended all primary nickel production in Sudbury at the end of 2008 due to low metal prices, but its current operations still contain extensive nickel- rich orebodies, which provide upside exposure to the nickel price in the future.



FNX has a strong balance sheet to support its growth, including $258.2 million in cash, $151.1 million in investments and zero debt as at September 30, 2009.








Click here to go to DMC Mining's page


